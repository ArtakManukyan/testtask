import * as React from 'react';
import Post from "../../components/util/post";
import {Grid} from "@material-ui/core";
import {Pagination} from "@mui/material";
import {useRouter} from 'next/router'

export default function Posts({posts, pageCount, page}) {

    const router = useRouter()
    const listItems = posts.map((post) => {
        return (
            <Post key={post._id} id={post._id} author={post.author} text={post.text} date={post.date}/>
        )
    });

    const handleChange = (event, page) => {
        router.push(
            {
                pathname: 'posts',
                query: {
                    page
                }
            }
        )
    };

    return (
        <>
            <Grid container spacing={2} justifyContent="center">
                <Grid item xs={8}>
                    {listItems}
                </Grid>
            </Grid>
            <div className="pagination">
                <Pagination count={pageCount} page={page} color="primary" onChange={handleChange}/>
            </div>
        </>
    );
}

export async function getServerSideProps(context) {

    const res = await fetch(`${process.env.API_URI}posts?page=${context.query.page ?? 1}`)
    const data = await res.json()

    return {
        props: {
            posts: data.posts,
            pageCount: data.pageCount,
            page: data.page,
        }
    }
}