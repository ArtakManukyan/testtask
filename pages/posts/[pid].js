import * as React from 'react';
import {useRouter} from 'next/router';
import {Grid} from "@material-ui/core";
import {Pagination} from "@mui/material";
import Comment from "../../components/util/comment";

export default function Post({id, comments, pageCount, page}) {

    const router = useRouter()

    const listItems = comments.map((post) => {
        return (
            <Comment key={post._id} author={post.author} text={post.text}/>
        )
    });

    const handleChange = (event, page) => {
        router.push(
            {
                pathname: `/posts/${id}`,
                query: {
                    page
                }
            }
        )
    };

    return (
        <>
            <Grid container spacing={2} justifyContent="center">
                <Grid item xs={8}>
                    {listItems}
                </Grid>
            </Grid>
            <div className="pagination">
                <Pagination count={pageCount} page={page} color="primary" onChange={handleChange}/>
            </div>
        </>
    );
}


export async function getServerSideProps(context) {

    const res = await fetch(`${process.env.API_URI}posts/${context.query.pid}?page=${context.query.page ?? 1}`)
    const data = await res.json()


    return {
        props: {
            comments: data.comments,
            pageCount: data.pageCount,
            page: data.page,
            id: context.query.pid
        }
    }
}
