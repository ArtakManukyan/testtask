import Comment from '../../../server/Models/Comment';
import dbConnect from '../../../server/lib/dbConnect';

export default async function handler(req, res) {

    await dbConnect()

    const limit = 10;
    const page = parseInt(req.query.page);

    const comments = await Comment.find({_post: req.query.id}).sort({date:1}).skip((page - 1) * limit).limit(limit);

    const commentsCount = await Comment.count({_post: req.query.id});

    res.status(200).json({
        comments,
        pageCount: Math.ceil(commentsCount / 10),
        page
    });
}