import Post from '../../../server/Models/Post';
import dbConnect from '../../../server/lib/dbConnect';

export default async function handler(req, res) {

    await dbConnect()

    const limit = 10;
    const page = parseInt(req.query.page);

    const posts = await Post.find({}).sort({date:1}).skip((page - 1) * limit).limit(limit);
    const postsCount = await Post.count({});

    res.status(200).json({
        posts,
        pageCount: Math.ceil(postsCount / 10),
        page
    });
}