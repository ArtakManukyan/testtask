import mongoose from 'mongoose'
const PostSchema = new mongoose.Schema({
    title: String,
    text: String,
    date: Date,
    comments:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }]
})

module.exports = mongoose.models.Post || mongoose.model('Post', PostSchema)