import mongoose from 'mongoose'
const CommentSchema = new mongoose.Schema({
    author: String,
    text: String,
    date: Date,
    _post: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post'
    }
})

module.exports = mongoose.models.Comment ||  mongoose.model('Comment', CommentSchema)