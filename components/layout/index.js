import Header from "../util/Header";

export default function layout({children})
{
    return(
        <>
            <Header/>
            {children}
        </>
    )
}