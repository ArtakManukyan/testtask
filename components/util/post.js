import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {CardActionArea} from '@mui/material';
import Link from 'next/link';
import moment from 'moment';
export default function Post({id, text, author, date}) {
    return (
        <>
            <Card sx={{minWidth: 345, margin: 1}}>
                <CardActionArea>
                    <Link href={`posts/${id}`}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {author}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {text}
                            </Typography>
                            <Typography variant="body2" color="text.secondary" className="date">
                                {moment(date).format('LLLL')}
                            </Typography>
                        </CardContent>
                    </Link>

                </CardActionArea>
            </Card>
        </>
    );
}
